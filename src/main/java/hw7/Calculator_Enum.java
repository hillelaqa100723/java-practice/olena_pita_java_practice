package hw7;
import java.util.List;
public class Calculator_Enum {
    enum Operator {
        PLUS("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE("/");

        private final String symbol;

        Operator(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }
    }

    public class enumcalc {
        public static void main(String[] args) {
            List<String> inputData = List.of("5", "PLUS", "5", "MINUS", "3", "MULTIPLY", "2", "DIVIDE", "1");
            Double result = calculate(inputData);
            String resultString = prepareResultString(inputData, result);
            System.out.println(resultString);
        }

        public static Double calculate(List<String> data) {
            Double result = null;
            if (data.size() % 2 != 1) {
                System.out.println("Input data format is invalid.");
                return null;
            }

            try {
                result = Double.parseDouble(data.get(0));
                for (int i = 1; i < data.size(); i += 2) {
                    Operator operator = Operator.valueOf(data.get(i));
                    Double operand = Double.parseDouble(data.get(i + 1));

                    switch (operator) {
                        case PLUS:
                            result += operand;
                            break;
                        case MINUS:
                            result -= operand;
                            break;
                        case MULTIPLY:
                            result *= operand;
                            break;
                        case DIVIDE:
                            if (operand != 0) {
                                result /= operand;
                            } else {
                                System.out.println("Cannot divide by zero.");
                                return null;
                            }
                            break;
                    }
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid number format.");
                return null;
            } catch (IllegalArgumentException e) {
                System.out.println("Invalid argument.");
                return null;
            }

            return result;
        }

        public static String prepareResultString(List<String> data, Double result) {
            StringBuilder resultString = new StringBuilder();
            resultString.append(data.get(0));
            for (int i = 1; i < data.size(); i += 2) {
                resultString.append(Operator.valueOf(data.get(i)).getSymbol()).append(data.get(i + 1));
            }
            resultString.append("=").append(result);
            return resultString.toString();
        }
    }
}



