package hw6;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Lottery2 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        for (int i = 1; i <= 36; i++) {
            numbers.add(i);
        }

        List<Integer> selectedNumbers = new ArrayList<>();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            int index = random.nextInt(numbers.size());
            selectedNumbers.add(numbers.get(index));
            numbers.remove(index);
        }

        System.out.print("Випадкова вибірка 6 чисел з послідовності 1-36: ");
        for (int number : selectedNumbers) {
            System.out.print(number + " ");
        }
    }
}
