package hw8;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CalculatorWithFile {
    enum Operator {
        PLUS("+"),
        MINUS("-"),
        MULTIPLY("*"),
        DIVIDE("/");


        private final String symbol;

        Operator(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }
    }

    public static void main(String[] args) {
        File dataFile = new File("/Users/admin/olena_pita_java_practice/files/calculator.txt");
        List<String> inputData = getDataFromFile(dataFile);
        if (inputData != null) {
            Double result = calculate(inputData);
            if (result != null) {
                String resultString = prepareResultString(inputData, result);
                System.out.println(resultString);
            } else {
                System.out.println("Calculation error");
            }
        } else {
            System.out.println("Invalid data format");
        }
    }

    public static List<String> getDataFromFile(File dataFile) {
        List<String> data = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(dataFile))) {
            String line = reader.readLine();
            if (line != null) {
                String[] tokens = line.split(", ");
                for (String token : tokens) {
                    data.add(token);
                }
                return data;
            } else {
                System.out.println("File is empty");
                return null;
            }
        } catch (IOException e) {
            return null;
        }
    }

    public static Double calculate(List<String> data) {
        Double result = null;
        if (data.size() != 3) {
            return null;
        }

        try {
            Double operand1 = Double.parseDouble(data.get(0));
            Operator operator = Operator.valueOf(data.get(1));
            Double operand2 = Double.parseDouble(data.get(2));

            switch (operator) {
                case PLUS:
                    result = operand1 + operand2;
                    break;
                case MINUS:
                    result = operand1 - operand2;
                    break;
                case MULTIPLY:
                    result = operand1 * operand2;
                    break;
                case DIVIDE:
                    if (operand2 != 0) {
                        result = operand1 / operand2;
                    } else {
                        System.out.println("Cannt div on 0");
                        return null;
                    }
                    break;
            }
        } catch (NumberFormatException e) {
            System.out.println("Wrong format");
            return null;
        } catch (IllegalArgumentException e) {
            System.out.println("Wrong operator");
            return null;
        }

        return result;
    }

    public static String prepareResultString(List<String> data, Double result) {
        StringBuilder resultString = new StringBuilder();
        String operationSymbol = data.get(1);
        return data.get(0) + " " + operationSymbol + " " + data.get(2) + " = " + result;
    }
}
