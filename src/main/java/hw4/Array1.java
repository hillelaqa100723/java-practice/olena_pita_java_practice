package hw4;

public class Array1 {

        public static void printMainDiagonal(int[][] matrix) {
            System.out.print("Головна діагональ: ");
            for (int i = 0; i < matrix.length; i++) {
                System.out.print(matrix[i][i]);
                if (i != matrix.length - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }

        public static void printSecondaryDiagonal(int[][] matrix) {
            System.out.print("Побічна діагональ: ");
            for (int i = 0; i < matrix.length; i++) {
                System.out.print(matrix[i][matrix.length - 1 - i]);
                if (i != matrix.length - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }

        public static void main(String[] args) {
            int[][] matrix = {
                    {1, 2, 3},
                    {4, 5, 6},
                    {7, 8, 9}
            };

            System.out.println("Матриця:");
            for (int[] row : matrix) {
                for (int value : row) {
                    System.out.print(value + " ");
                }
                System.out.println();
            }

            printMainDiagonal(matrix);
            printSecondaryDiagonal(matrix);
        }
    }
