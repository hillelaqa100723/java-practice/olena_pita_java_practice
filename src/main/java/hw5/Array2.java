package hw5;

public class Array2 {
        public static int[] sumsInRows(int[][] source) {
            int[] sums = new int[source.length];

            for (int i = 0; i < source.length; i++) {
                int sum = 0;
                for (int j = 0; j < source[i].length; j++) {
                    sum += source[i][j];
                }
                sums[i] = sum;
            }

            return sums;
        }

        public static void main(String[] args) {
            int[][] matrix = {
                    {1, 2, 3},
                    {4, 5, 6},
                    {7, 8, 9}
            };

            System.out.println("Матриця:");
            for (int[] row : matrix) {
                for (int value : row) {
                    System.out.print(value + " ");
                }
                System.out.println();
            }

            int[] sums = sumsInRows(matrix);
            System.out.print("Результат: ");
            for (int sum : sums) {
                System.out.print(sum + " ");
            }
        }
    }
