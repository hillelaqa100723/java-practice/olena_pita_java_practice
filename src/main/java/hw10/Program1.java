package hw10;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

public class Program1 {

    public static void main(String[] args) throws IOException {

        File dataFile = new File("files/data.csv");
        Map<Integer, User1> userData = getDataFromFile(dataFile);


        int selectedId = 3522;
        User1 user = getDataById(userData, selectedId);
        System.out.println("User with selected ID " + selectedId + ": " + user);

        System.out.println();


        String lastNameToCount = "Petrov";
        int numberOfOccurrences = getNumberOfOccurrences(userData, lastNameToCount);
        System.out.println("Number of occurrences of '" + lastNameToCount + "': " + numberOfOccurrences);

        System.out.println();


        int ageToCompare = 30;
        Map<Integer, User1> usersAgeMoreThan30 = getUsersAgeMoreThen(userData, ageToCompare);

        System.out.println("Users older than " + ageToCompare + ":");
        for (Map.Entry<Integer, User1> entry : usersAgeMoreThan30.entrySet())

    {
        Integer userId = entry.getKey();
        User1 userEntry = entry.getValue();
        System.out.println("User ID: " + userId + ", User Data: " + userEntry);
    }

        System.out.println();


        Map<String, List<Integer>> equalUsers = findEqualUsers(userData);

        System.out.println("Equal Users:");
        for (
                Map.Entry<String, List<Integer>> entry : equalUsers.entrySet()) {
            String fullName = entry.getKey();
            List<Integer> ids = entry.getValue();

            System.out.print(fullName + ": ");
            for (int id : ids) {
                System.out.print(id + " ");
            }
            System.out.println();
        }

    }

    public static Map<Integer, User1> getDataFromFile(File dataFile) {
        Map<Integer, User1> mapData = new HashMap<>();

        try {
            List<String> lines = FileUtils.readLines(dataFile, Charset.defaultCharset());
            for (String srt : lines) {
                String[] split = srt.split(",");

                Integer id = Integer.parseInt(split[0].trim());
                String lastName = split[1];
                String firstName = split[2];
                Integer age = Integer.parseInt(split[3].trim());

                User1 user = new User1(id, firstName, lastName, age);
                mapData.put(id, user);

            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return mapData;
    }


    public static User1 getDataById(Map<Integer, User1> mapData, Integer id) {
        return mapData.get(id);
    }

    public static int getNumberOfOccurrences(Map<Integer, User1> mapData, String lastName) {
        int count = 0;
        for (User1 user : mapData.values()) {
            String userLastName = user.getLastName().trim();
            if (userLastName.equals(lastName.trim())) {
                count++;
            }
        }
        return count;
    }

    public static Map<Integer, User1> getUsersAgeMoreThen(Map<Integer, User1> mapData, int age) {
        Map<Integer, User1> result = new HashMap<>();

        for (Map.Entry<Integer, User1> entry : mapData.entrySet()) {
            if (entry.getValue().getAge() > age) {
                result.put(entry.getKey(), entry.getValue());
            }
        }

        return result;
    }


    static Map<String, List<Integer>> findEqualUsers(Map<Integer, User1> users) {
        Map<String, List<Integer>> result = new HashMap<>();

        for (User1 user : users.values()) {
            String fullName = user.getFirstName() + " " + user.getLastName();

            if (result.containsKey(fullName)) {
                result.get(fullName).add(user.getId());
            } else {
                List<Integer> idList = new ArrayList<>();
                idList.add(user.getId());
                result.put(fullName, idList);
            }
        }
        result.entrySet().removeIf(entry -> entry.getValue().size() <= 1);
        return result;
    }
}