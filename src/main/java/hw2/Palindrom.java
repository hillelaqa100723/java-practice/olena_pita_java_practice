package hw2;
public class Palindrom {

        public static boolean isPalindrome(String input) {
            String processedInput = input.replaceAll("\\s+", "").toLowerCase();

            int left = 0;
            int right = processedInput.length() - 1;
            while (left < right) {
                if (processedInput.charAt(left) != processedInput.charAt(right)) {
                    return false;
                }
                left++;
                right--;
            }
            return true;
        }

        public static void main(String[] args) {
            String input = "кит на морі романтик";
            boolean result = isPalindrome(input);
            System.out.println("Чи є рядок паліндромом? " + result);
        }
    }



