package hw9;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class hw9 {
    public static void main(String[] args) {
        File dataFile = new File("/Users/admin/olena_pita_java_practice/files/data.txt");
        Map<Integer, String> mapData = getDataFromFile(dataFile);
        if (mapData != null) {

            Integer idToFind = 9635;
            String foundData = getDataById(mapData, idToFind);
            System.out.println("Data for ID " + idToFind + ": " + foundData);

            String lastNameToCount = "Petrov";
            int occurrences = getNumberOfOccurrences(mapData, lastNameToCount);
            System.out.println("LastNames numbers for " + lastNameToCount + ": " + occurrences);
        } else {
            System.out.println("Wrong file data");
        }
    }

    public static Map<Integer, String> getDataFromFile(File dataFile) {
        Map<Integer, String> mapData = new HashMap<>();
        try {
            List<String> lines = FileUtils.readLines(dataFile, "UTF-8");
            for (String line : lines) {
                String[] parts = line.split(",");
                if (parts.length == 2) {
                    Integer id = Integer.parseInt(parts[0].trim());
                    String data = parts[1].trim();
                    mapData.put(id, data);
                }
            }
            return mapData;
        } catch (IOException e) {
            System.out.println("Error " + e.getMessage());
            return null;
        }
    }

    public static String getDataById(Map<Integer, String> mapData, Integer id) {
        return mapData.get(id);
    }

    public static int getNumberOfOccurrences(Map<Integer, String> mapData, String lastName) {
        int count = 0;
        List<String> list = new ArrayList<>(mapData.values());
        for (String fullName : list) {
            if (fullName.split(" ")[0].equals(lastName)) count++;
        }

        return count;
    }

}
